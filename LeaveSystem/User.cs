﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeaveSystem
{
    //POCO
    //Plain Old C# Object
    public class User
    {
        public string Name { get; set; }
        public double LeaveDays { get; set; }
    }
}
