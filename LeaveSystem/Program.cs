﻿using System;

namespace LeaveSystem
{
    class Program
    {
        //I have changed leaveprompt to be an object
        static LeavePrompt leavePrompt = new LeavePrompt();
        static void Main(string[] args)
        {
            //When you do this, make sure there is a way to exit
            //I.E "return"
            while(true)
            {
                leavePrompt.userSelection();
                var resultOfSelection = leavePrompt.selection();
                if(resultOfSelection == false) return;
            }
        }
    }
}
