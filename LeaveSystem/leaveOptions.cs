﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeaveSystem
{
    class leaveOptions
    {
        public static float leaveAmount = 15f;
        public static float sickLeaveAmount = 30f;
        public static float leaveSubtract;
        public static float leaveTaken = 0f;

        //Checking the Leave Amount of user
        public static float leaveTotal()
        {
            Console.Write("Your total Leave Days is " + leaveAmount + "\n");
            return leaveAmount;
        }

        //submission of leave days for user
        public static float submitLeave()
        {
            //subMenuOptions();
            //int subMenuSelection = Convert.ToInt32(Console.ReadLine);
            //switch (subMenuSelection)
            //{
            //    //case 1:
            //        //break;
            //}
            Console.WriteLine("Please enter the amount of Leave to submit");
            float leaveInput = float.Parse((Console.ReadLine()));
            if (leaveInput <= leaveAmount)
            {
                leaveSubtract += leaveInput;
                leaveAmount -= leaveInput;
                Console.WriteLine(leaveSubtract + " days added to your Leave request. You now have " + leaveAmount +
                    " days left for the year");
            }
            else
            {
                Console.WriteLine("Not enough Leave days to accommodate your request. Please speak to your supervisor");
            }
            return 1;
        }

        //Checking the amount of Leave days taken for user
        public static float leaveSchedule()
        {
            Console.WriteLine("You have taken " + leaveSubtract + " days Leave for the current year");
            return leaveSubtract;
        }

        //Menu option of Leave System for User to select.
        public static void menuOptions()
        {
            Console.WriteLine("Please select one of the following options:");
            Console.WriteLine("1. Check Leave Total");
            Console.WriteLine("2. Submit Leave");
            Console.WriteLine("3. Check Leave Schedule");
            Console.WriteLine("4. Exit Leave System");

        }

        static void subMenuOptions()
        {
            Console.WriteLine("What type of Leave would you like to submit");
            Console.WriteLine("1. Annual Leave");
            Console.WriteLine("2. Sick Leave");
            Console.WriteLine("3. Maternity Leave");
            Console.WriteLine("4. Family Responsibility Leave");
        }
    }
}
