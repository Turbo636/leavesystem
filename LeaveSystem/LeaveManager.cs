﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeaveSystem
{
    public class LeaveManager
    {
        //Composition
        public static List<User> users = new List<User>();
        //List<double> dblList = new List<double>();
        public LeaveManager()
        {
            
        }
        public bool ApproveLeave(string name, double requestedLeave = 3.5)
        {
            var user = users.FirstOrDefault(x => x.Name == name);
            if(user != null)
            {
                user.LeaveDays -= requestedLeave;
                return true;
            }
            else
            {
                return false;
            }
        }

        /*
         * Having problems in listing users. 
         */
        public void ListUsers()
        {
            //Here I am printing the name and leave number of what I pass in
            //We print the data here, so we don't need to return string either
            //Console.WriteLine(string.Format($"{Name}  {initialLeave}"));

            //To print my users, I need to look at my list "users"
            //This is the same list we added to in the "AddUser" method below
            /*
                users.Add(
                    new User()
                    {
                        Name = name,
                        LeaveDays = initialLeave
                    }
                );
            */
            
            //Many ways to skin a cat
            /*
                foreach(var user in users)
                {
                    Console.WriteLine(String.Format($"{user.Name}  {user.LeaveDays}"));
                }
                for(int i=0;i<users.length;i++)
                {
                    Console.WriteLine(String.Format($"{user.Name}  {user.LeaveDays}"));
                }
                users.ForEach(user => {
                    Console.WriteLine(String.Format($"{user.Name}  {user.LeaveDays}"));
                });
            */

            users.ForEach(user => {
                Console.WriteLine(String.Format($"{user.Name}\t\t  {user.LeaveDays}"));
            });
            
            //Name is a string - therefore
            //(Name).ToList <-- String.ToList is a list<char> .. example "Tiaan" == List<char>(){'T','i','a','a','n'}

            //return (Name).ToList<User>;// users.ForEach(user => Console.Write(user));
        }
        public bool AddUser(string name, double initialLeave = 15)
        {
            try
            {
                users.Add(
                    new User()
                    {
                        Name = name,
                        LeaveDays = initialLeave
                    }
                );
                return true;
            }
            catch
            {
                return false;
            }
        }
        public bool RemoveUser(string Name)
        {
            /*
             * BASIC LOGIC
            var user = users.FirstOrDefault(x => x.Name == Name);
            if (user != null)
            {
                users.RemoveAll(x => x.Name == Name);
                return true;
            }
            else
            {
                return false;
            }
            */
            /*
             * SIMPLIFICATION #1
            int removedCount = users.RemoveAll(x => x.Name == Name);
            return removedCount > 0;
            */
            // SIMIPLIFICATION #2
            return (users.RemoveAll(x => x.Name == Name)) > 0;
        }

    }
}
