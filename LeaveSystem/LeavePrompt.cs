﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeaveSystem
{
    class LeavePrompt
    {
        LeaveManager manager = new LeaveManager();
        public string leavePrompt()
        {
            string prompt = "This is LeavePrompt";
            Console.WriteLine(prompt);
            return prompt;
        }

        public void userSelection()
        {
            Console.WriteLine("Please select one of the following: ");
            Console.WriteLine("1. Manager");
            Console.WriteLine("2. Employee");
            Console.WriteLine("3. HR");
            Console.WriteLine("4. Exit");
        }

        public void managerSubMenu()
        {
            Console.WriteLine("Please select one of the following: ");
            Console.WriteLine("1. List Users");
            Console.WriteLine("2. Approve Leave Request");
            Console.WriteLine("3. Add Users");
            Console.WriteLine("4. Return to Main Menu");
        }

        public bool selection()
        {

            int selection = int.Parse(Console.ReadLine());

            switch (selection)
            {
                case 1:
                    Console.WriteLine("Manager Selected");
                    Console.WriteLine("Please enter the password for Manager");
                    //This manager will not exist after the function is completed
                    //LeaveManager manager = new LeaveManager();
                    var password = Console.ReadLine();
                    if (password == "Manager")
                    {

                        managerSubMenu();
                        int subMenuSelection = int.Parse(Console.ReadLine());
                        
                        //here i set my initial name and number
                        var name = "";
                        int number = 0;
                        switch (subMenuSelection)
                        {
                            case 1:
                                Console.WriteLine("Name\t\t" + "number");
                                //Here I am printing the current value of name and number
                                manager.ListUsers();
                                //Console.WriteLine(manager.ListUsers(name + "\t\t", number));
                                //manager.ListUsers(name, number);
                                break;
                            case 2:

                                /*
                                 * Code need refactoring. Trying to approve leave based on user input name.
                                 * 
                                 * */
                                Console.WriteLine("Which user do you want to approve leave?");
                                manager.ListUsers();
                                var userSelection = (string.Format($"{name}"));//LeaveManager.users.All(x => x.Name == name).ToString();
                                var userLeaveApprove = Console.ReadLine();
                                if(userLeaveApprove == userSelection)
                                {
                                    manager.ApproveLeave(name, number);
                                    
                                }
                                
                                break;
                            case 3:
                                //Here i set 1 value for name and number
                                //When I add a new user, it changes
                                Console.WriteLine("Enter The new user name");
                                name = Console.ReadLine();
                                number = 0;
                                Console.WriteLine($"How many leave days does {name} get?");
                                if (int.TryParse(Console.ReadLine(), out number))
                                {                              
                                    manager.AddUser(name, number);
                                    //Console.WriteLine("Name" + "  " + "number");
                                    //Console.WriteLine(manager.ListUsers(name + "\t\t", number));
                                    manager.ListUsers();
                                }
                                else
                                {
                                    Console.WriteLine("You did not enter a number fool! Try again.");
                                }
                                return true;
                        }
                    } 
                    else
                    {
                        Console.WriteLine("Incorrect password.");
                    }
                    return true;
                case 2:
                    Console.WriteLine("Employee Selected");
                    return false;
                case 3:
                    Console.WriteLine("HR Selected");
                    return false;
                case 4:
                    Console.WriteLine("Goodbye!");
                    return false;
                default:
                    //Invalid option, just repeat
                    return true;
            }
        }
    }
}
